# api-stm

## Run Local Server

Follow the steps below.

```
$ cd api-stm
$ python -m venv venv
$ source ./venv/Scripts/activate
$ pip install -r requirements.txt
$ uvicorn app.main:app --reload
```

Go to http://127.0.0.1/docs 


## Quick Deploy with Container



1. Build the docker image for the STM FastAPI app

```
$ cd api-stm
$ docker build -t my_stm_app_image .
```

2. Start the docker container

```
$ docker run -d --name my_stm_app_container -p 80:80 my_stm_app_image
```

3. Check the app doc for usage examples at http://127.0.0.1/docs 

4. Stop the container with

```
$ docker stop my_stm_app_container
```


## Test Coverage

Check test coverage with

```
$ pytest --cov-report term-missing --cov=app app/tests
```

Note: this will set up and tear down a mock database.
