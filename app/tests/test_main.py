import os

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ..database import Base
from ..main import app, get_db
from ..schemas import CDM

SQLALCHEMY_MOCK_DATABASE_URL = "sqlite:///./test.db"

engine = create_engine(
    SQLALCHEMY_MOCK_DATABASE_URL, connect_args={"check_same_thread": False}
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)


def setup_mock_db(db):
    # ObjectDictionary
    sql_mock_object1_id = (
        "INSERT OR REPLACE INTO object_dictionary(object_id,norad_id,cospar_id) "
        "VALUES ('74PR7J3R80',1,'xyz')"
    )
    db.execute(sql_mock_object1_id)
    db.commit()

    sql_mock_object2_id = (
        "INSERT OR REPLACE INTO object_dictionary(object_id,norad_id,cospar_id) "
        "VALUES ('JFB81Z46C5',2,'xyz')"
    )
    db.execute(sql_mock_object2_id)
    db.commit()

    # Object
    sql_mock_object = (
        "INSERT OR REPLACE INTO object (object_id,name,class,launch_date,radar_cross_section_size,period,manoeuvrable,life_expectancy,avg_life_loss_per_sec_in_manoeuvre) "
        "VALUES ('74PR7J3R80','','','','',98.94,1,'',20);"
    )
    db.execute(sql_mock_object)
    db.commit()

    # CDM
    sql_mock_cdm1 = (
        "INSERT OR REPLACE INTO cdm(cdm_id,creation,time_closest_approach,miss_distance,probability_collision,obj1_id,obj2_id,source) "
        "VALUES (676007076,'2022-11-01 15:49:01','2022-11-02 04:57:03.446',457,0.000250118348878494,'74PR7J3R80','JFB81Z46C5','C');"
    )
    db.execute(sql_mock_cdm1)
    db.commit()

    sql_mock_cdm2 = (
        "INSERT OR REPLACE INTO cdm(cdm_id,creation,time_closest_approach,miss_distance,probability_collision,obj1_id,obj2_id,source) "
        "VALUES (676007077,'2022-11-01 15:50:01','2022-11-02 04:57:03.446',456,0.0003,'74PR7J3R80','JFB81Z46C5','B');"
    )
    db.execute(sql_mock_cdm2)
    db.commit()

    # Action
    sql_mock_action = (
        "INSERT OR REPLACE INTO action(action_id,object_id,action_type,expected_start,expected_end,executed_start,executed_end) "
        " VALUES (1,'JFB81Z46C5','Collision Avoidance','2022-11-11 10:35:44.000012','2022-11-11 10:37:44.000012','2022-11-11 10:36:14.000012','2022-11-11 10:38:17.000012')"
    )
    db.execute(sql_mock_action)
    db.commit()


def override_get_db():
    db = TestingSessionLocal()
    try:
        setup_mock_db(db)
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


# Object
def test_read_object_valid_object_id():

    response = client.get("/object/74PR7J3R80")
    assert response.status_code == 200


def test_read_object_invalid_object_id():

    response = client.get("/object/foobar")
    assert response.status_code == 404


# ObjectDictionary
def test_read_object_ids_valid_object_id():

    response = client.get("/object-ids/JFB81Z46C5")
    assert response.status_code == 200


def test_read_object_id_invalid_object_id():

    response = client.get("/object-ids/foobar")
    assert response.status_code == 404


# CDM
def test_read_valid_cdm():
    response = client.get("/cdm/676007076")
    assert response.status_code == 200


def test_read_invalid_cdm():
    response = client.get("/cdm/123456")
    assert response.status_code == 404


def test_get_invalid_object_cdms():
    response = client.get("/cdms/123456ABC")
    assert response.status_code == 404


def test_get_valid_object_cdms():
    response = client.get("/cdms/74PR7J3R80")
    assert response.status_code == 200

    # There are 2 CDMs associated with this object
    assert len(response.json()) == 2


def test_get_valid_pair_cdms():
    obj1 = "74PR7J3R80"
    obj2 = "JFB81Z46C5"

    response = client.get("/cdms/{0}/{1}".format(obj1, obj2))
    assert response.status_code == 200

    response = client.get("/cdms/{0}/{1}".format(obj2, obj1))
    assert response.status_code == 200


def test_get_invalid_pair_cdms():
    response = client.get("/cdms/{0}/{1}".format("3333", "44444"))
    assert response.status_code == 404


def test_create_new_cdm():
    response = client.post(
        "/cdms/",
        json={
            "cdm_id": 0,
            "creation": "string",
            "time_closest_approach": "string",
            "miss_distance": 0,
            "probability_collision": 0,
            "object_1_id": "string",
            "object_2_id": "string",
            "source_name": "string",
        },
    )
    assert response.status_code == 200


def test_delete_cdm():
    response = client.delete("/cdm/0")
    assert response.status_code == 200


def test_delete_nonexisting_cdm():
    response = client.delete("/cdm/456")
    assert response.status_code == 404


def test_create_existing_cdm():

    response = client.post(
        "/cdms/",
        json={
            "cdm_id": 676007076,
            "creation": "string",
            "time_closest_approach": "string",
            "miss_distance": 0,
            "probability_collision": 0,
            "object_1_id": "string",
            "object_2_id": "string",
            "source_name": "string",
        },
    )
    assert response.status_code == 400


# Action
def test_read_valid_action():
    response = client.get("/action/1")
    assert response.status_code == 200


def test_read_invalid_action():
    response = client.get("/action/123")
    assert response.status_code == 404


def test_create_new_action():
    response = client.get("/action/321")
    assert response.status_code == 404

    response = client.post(
        "/actions/",
        json={
            "action_id": 321,
            "object_id": "string",
            "action_type": "string",
            "expected_start": "string",
            "expected_end": "string",
            "executed_start": "string",
            "executed_end": "string",
        },
    )
    assert response.status_code == 200

    response = client.get("/action/321")
    assert response.status_code == 200


def test_create_existing_action():
    response = client.post(
        "/actions/",
        json={
            "action_id": 321,
            "object_id": "string",
            "action_type": "string",
            "expected_start": "string",
            "expected_end": "string",
            "executed_start": "string",
            "executed_end": "string",
        },
    )
    assert response.status_code == 400


def test_update_action():

    updated_values = {
        "action_id": 321,
        "object_id": "string",
        "action_type": "string",
        "expected_start": "string",
        "expected_end": "string",
        "executed_start": "updated",
        "executed_end": "updated",
    }

    response = client.put(
        "/action/",
        json=updated_values,
    )
    assert response.status_code == 200

    # Assert that values have been changed
    response = client.get("/action/321")
    assert response.json() == updated_values


def test_update_nonexisting_action():
    response = client.put(
        "/action/",
        json={
            "action_id": 2,
            "object_id": "string",
            "action_type": "string",
            "expected_start": "string",
            "expected_end": "string",
            "executed_start": "updated",
            "executed_end": "updated",
        },
    )
    assert response.status_code == 404


# Delete mock database
def test_db_session():
    engine.dispose()
    os.remove("./test.db")
