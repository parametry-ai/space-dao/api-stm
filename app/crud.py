from sqlalchemy.orm import Session
from . import models, schemas

def get_orbital_object(db: Session, object_id: str):
    return db.query(models.OrbitalObject).filter(models.OrbitalObject.object_id == object_id).first()

def get_orbital_objects(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.OrbitalObject).offset(skip).limit(limit).all()

def get_cdm(db: Session, cdm_id: int):
    return db.query(models.CDM).filter(models.CDM.cdm_id == cdm_id).first()

def get_cdms(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.CDM).offset(skip).limit(limit).all()

def get_action(db: Session, action_id: int):
    return db.query(models.Action).filter(models.Action.action_id == action_id).first()

def get_actions(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Action).offset(skip).limit(limit).all()

def get_orbital_object_ids(db: Session, object_id: str):
    return db.query(models.ObjectDictionary).filter(models.ObjectDictionary.object_id == object_id).first()

def get_cdms_from_object(db: Session, object_1_id: str, skip: int = 0, limit: int = 100):
    return db.query(models.CDM).filter((models.CDM.object_1_id == object_1_id) | (models.CDM.object_2_id == object_1_id)).offset(skip).limit(limit).all()

def get_cdms_from_pair_objects(db: Session, object_1_id: str, object_2_id: str, skip: int = 0, limit: int = 100):
    return db.query(models.CDM).filter(((models.CDM.object_1_id == object_1_id) & (models.CDM.object_2_id == object_2_id)) | ((models.CDM.object_1_id == object_2_id) & (models.CDM.object_2_id == object_1_id))).offset(skip).limit(limit).all()

def create_cdm(db: Session, cdm: schemas.CDM):
    db_cdm = models.CDM(cdm_id=cdm.cdm_id, creation=cdm.creation, time_closest_approach=cdm.time_closest_approach,
                        miss_distance=cdm.miss_distance, probability_collision=cdm.probability_collision,
                        object_1_id=cdm.object_1_id, object_2_id=cdm.object_2_id, source_name=cdm.source_name)
    db.add(db_cdm)
    db.commit()
    db.refresh(db_cdm)
    return db_cdm

def delete_cdm(db: Session, cdm: models.CDM):
    db.delete(cdm)
    db.commit()
    return "CDM removed from database."

def create_action(db: Session, action: schemas.Action):
    db_action = models.Action(action_id=action.action_id, object_id=action.object_id, action_type=action.action_type,
                              expected_start=action.executed_start, expected_end=action.expected_end,
                              executed_start=action.executed_start, executed_end=action.executed_end)
    db.add(db_action)
    db.commit()
    db.refresh(db_action)
    return db_action

def update_action(db: Session, db_action: schemas.Action, action: schemas.Action):
    db_action.executed_start = action.executed_start
    db_action.executed_end = action.executed_end
    db.commit()
    return db_action
