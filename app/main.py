from datetime import datetime
from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    """ Creates a new SQLALCHEMY SessionLocal (class created in database.py) that will be used
        in a single request, then closes it once the request is finished."""
    database = SessionLocal()
    try:
        yield database
    finally:
        database.close()

@app.get("/object/{object_id}", response_model=schemas.OrbitalObject)
def read_object(object_id: str, database: Session = Depends(get_db)):
    """ Get orbital object through its ID """
    db_orbital_object = crud.get_orbital_object(database, object_id)
    if db_orbital_object is None:
        raise HTTPException(status_code=404, detail="Object not found.")

    orbit_location = 1 # in LEO, by default
    if db_orbital_object.period >= 225:
        orbit_location = 2 # deep space
    
    db_orbital_object.period = orbit_location

    return db_orbital_object

# For testing purposes
@app.get("/objects/", response_model=List[schemas.OrbitalObject])
def read_objects(skip: int = 0, limit: int = 100, database: Session = Depends(get_db)):
    """ Get N orbital objects """
    orbital_objects = crud.get_orbital_objects(database, skip=skip, limit=limit)
    if orbital_objects is None:
        raise HTTPException(status_code=404, detail="No objects were found.")
    return orbital_objects

@app.get("/cdm/{cdm_id}", response_model=schemas.CDM)
def read_cdm(cdm_id: int, database: Session = Depends(get_db)):
    """ Get CDM through its ID"""
    db_cdm = crud.get_cdm(database, cdm_id)
    if db_cdm is None:
        raise HTTPException(status_code=404, detail="CDM not found.")

    # Transform property values so that a smart contract can use them.
    # Turn pc into a categorical.
    new_pc = 2
    if db_cdm.probability_collision < 0.0001:
        new_pc = 0
    elif db_cdm.probability_collision < 0.01:
        new_pc = 1

    db_cdm.probability_collision = new_pc

    # Compute time to tca in hours
    tca_datetime = datetime.strptime(db_cdm.time_closest_approach, "%Y-%m-%d %H:%M:%S.%f")
    time_now = datetime.now()
    time_difference_in_sec = (tca_datetime - time_now).total_seconds()
    time_difference_in_hrs = divmod(time_difference_in_sec, 3600)[0]
    
    db_cdm.time_closest_approach = int(time_difference_in_hrs)

    return db_cdm

# For testing purposes
@app.get("/cdms/", response_model=List[schemas.CDM])
def read_cdms(skip: int = 0, limit: int = 100, database: Session = Depends(get_db)):
    """ Get N CDMs """
    cdms = crud.get_cdms(database, skip=skip, limit=limit)
    if cdms is None:
        raise HTTPException(status_code=404, detail="No CDMs were found.")
    return cdms

@app.get("/action/{action_id}", response_model=schemas.Action)
def read_action(action_id: int, database: Session = Depends(get_db)):
    """ Get Action through its ID """
    db_action = crud.get_action(database, action_id)
    if db_action is None:
        raise HTTPException(status_code=404, detail="Action not found.")
    return db_action

# For testing purposes
@app.get("/actions/", response_model=List[schemas.Action])
def read_actions(skip: int = 0, limit: int = 100, database: Session = Depends(get_db)):
    """ Get N Actions """
    actions = crud.get_actions(database, skip=skip, limit=limit)
    if actions is None:
        raise HTTPException(status_code=404, detail="No Actions were found.")
    return actions

@app.get("/object-ids/{object_id}", response_model=schemas.ObjectDictionary)
def read_object_ids(object_id: str, database: Session = Depends(get_db)):
    """ Get Object's IDs through its Blockchain ID """
    db_object_ids = crud.get_orbital_object_ids(database, object_id)
    if db_object_ids is None:
        raise HTTPException(status_code=404, detail="Object not found.")
    return db_object_ids

@app.get("/cdms/{object_id}", response_model=List[schemas.CDM])
def get_object_cdms(object_id: str, skip: int = 0, limit: int = 100,  database: Session = Depends(get_db)):
    """ Get CDMs containing one orbital object """
    db_cdms_orbital_object = crud.get_cdms_from_object(database, object_id, skip=skip, limit=limit)
    if db_cdms_orbital_object == []:
        raise HTTPException(status_code=404, detail="No CDMs were found.")
    return db_cdms_orbital_object

@app.get("/cdms/{object_1_id}/{object_2_id}", response_model=List[schemas.CDM])
def get_pair_cdms(object_1_id: str, object_2_id: str, skip: int = 0, limit: int = 100,  database: Session = Depends(get_db)):
    """ Get CDMs from a pair of orbital objects """
    db_cdms_pair = crud.get_cdms_from_pair_objects(database, object_1_id, object_2_id, skip=skip, limit=limit)
    if db_cdms_pair == []:
        raise HTTPException(status_code=404, detail="No CDMs were found.")
    return db_cdms_pair

@app.post("/cdms/", response_model=schemas.CDM)
def create_cdm(cdm: schemas.CDM, database: Session = Depends(get_db)):
    """ Create new CDM """
    db_cdm = crud.get_cdm(database, cdm.cdm_id)
    if db_cdm:
        raise HTTPException(status_code=400, detail="There is already a CDM with this ID.")
    return crud.create_cdm(database, cdm)

@app.delete("/cdm/{cdm_id}")
def remove_cdm(cdm_id: int, database: Session = Depends(get_db)):
    """ Delete CDM through its ID"""
    db_cdm = crud.get_cdm(database, cdm_id)
    if db_cdm is None:
        raise HTTPException(status_code=404, detail="CDM not found.")
    delete_output = crud.delete_cdm(database, db_cdm)
    return delete_output

@app.post("/actions/", response_model=schemas.Action)
def create_action(action: schemas.Action, database: Session = Depends(get_db)):
    """ Create new Action """
    db_action = crud.get_action(database, action.action_id)
    if db_action:
        raise HTTPException(status_code=400, detail="There is already an Action with this ID.")
    return crud.create_action(database, action)

@app.put("/action/", response_model=schemas.Action)
def update_action(action: schemas.Action, database: Session = Depends(get_db)):
    """ Update Action with executed_start and executed_end """
    db_action = crud.get_action(database, action.action_id)
    if db_action is None:
        raise HTTPException(status_code=404, detail="Action not found.")
    return crud.update_action(database, db_action, action)
