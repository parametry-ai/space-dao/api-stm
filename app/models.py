from sqlalchemy import Column, ForeignKey, Integer, String, Float
from .database import Base

class OrbitalObject(Base):
    __tablename__ = "object"

    object_id = Column("object_id", String, primary_key=True)
    name = Column("name", String)
    class_name = Column("class", String)
    launch_date = Column("launch_date", String)
    radar_cross_section_size = Column("radar_cross_section_size", String)
    period = Column("period", Float)
    manoeuvrable = Column("manoeuvrable", Integer)
    life_expectancy = Column("life_expectancy", String)
    avg_life_loss_per_sec_in_manoeuvre = Column("avg_life_loss_per_sec_in_manoeuvre", Integer)

class CDM(Base):
    __tablename__ = "cdm"

    cdm_id = Column("cdm_id", Integer, primary_key=True)
    creation = Column("creation", String)
    time_closest_approach = Column("time_closest_approach", String)
    miss_distance = Column("miss_distance", Integer)
    probability_collision = Column("probability_collision", Float)
    object_1_id = Column("obj1_id", String, ForeignKey("object.object_id"))
    object_2_id = Column("obj2_id", String, ForeignKey("object.object_id"))
    source_name = Column("source", String)

class Action(Base):
    __tablename__ = "action"

    action_id = Column("action_id", Integer, primary_key=True)
    object_id = Column("object_id", String, ForeignKey("object.object_id"))
    action_type = Column("action_type", String)
    expected_start = Column("expected_start", String)
    expected_end = Column("expected_end", String)
    executed_start = Column("executed_start", String)
    executed_end = Column("executed_end", String)

class ObjectDictionary(Base):
    __tablename__ = "object_dictionary"

    object_id = Column("object_id", String, primary_key=True)
    norad_id = Column("norad_id", Integer)
    cospar_id = Column("cospar_id", String)
