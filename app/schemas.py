from typing import Union
from pydantic import BaseModel

class OrbitalObject(BaseModel):
    object_id: str
    name: str
    class_name: str
    launch_date: str
    period: float
    radar_cross_section_size: Union[str, None] = None
    manoeuvrable: int
    life_expectancy: Union[str, None] = None
    avg_life_loss_per_sec_in_manoeuvre: Union[int, None] = None

    class Config:
        orm_mode = True

class CDM(BaseModel):
    cdm_id: int
    creation: str
    time_closest_approach: str
    miss_distance: int
    probability_collision: float
    object_1_id: Union[str, None] = None
    object_2_id: Union[str, None] = None
    source_name: str

    class Config:
        orm_mode = True

class Action(BaseModel):
    action_id: int
    object_id: str
    action_type: str
    expected_start: str
    expected_end: str
    executed_start: Union[str, None] = None
    executed_end: Union[str, None] = None

    class Config:
        orm_mode = True

class ObjectDictionary(BaseModel):
    object_id: str
    norad_id: int
    cospar_id: str

    class Config:
        orm_mode = True
