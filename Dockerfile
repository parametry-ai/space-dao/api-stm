FROM python:3.10

RUN mkdir /stm_code

WORKDIR /stm_code

COPY ./requirements.txt /stm_code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /stm_code/requirements.txt

COPY ./app /stm_code/app

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
